<?php require_once 'preload.php'?>
<!DOCTYPE html>
<html>
<head>
	<title>KTHLabel รับพิมพ์ริบบิ้น ป้ายตราเสื้อ ริบบิ้นพิมพ์ ริบบิ้นผูกของขวัญ </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="Keywords" content="รับพิมพ์ริบบิ้น,ป้ายตราเสื้อ,ริบบิ้นพิมพ์	,พิมพ์ริบบิ้น,พิมพ์ตราเสื้อ,ริบบิ้นผูกของขวัญ,พิมพ์โลโก้">
	<meta name="Description" content="รับพิมพ์ริบบิ้น,ริบบิ้นพิมพ์,ริบบิ้นผ้า,โบว์พิมพ์ริบบิ้น,ป้ายทอตราเสื้อ โบว์ผูกของขวัญ">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="stats-in-th" content="f55e">
	<meta name="languege" content="Thai">
	<meta name="distribution" content="Global">
	<meta name="rating" content="General">
	<meta name="area" content="Creating">
	<meta name="resource-type" content="Document">
	<meta name="revisit-after" content="1 Days">
	<meta name="placename" content="Thailand">
	<meta name="expires" content="none">
	<meta http-equiv="cache-control" content="max-age=31557600" />
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/jquery.bxslider.css">
	<link rel="stylesheet" href="assets/css/main.css">
	<?php 
		if ( $detect->isMobile() ) {
			echo '<link rel="stylesheet" href="assets/css/mobile.css">';
		}
	?>
	<link rel="stylesheet" href="assets/css/product.css">
	<link rel="shortcut icon" href="assets/images/logo-black.ico">
	<!-- <script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-25373992-1', 'auto');
	ga('send', 'pageview');

	</script> -->
	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-TLBT7EYEND"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-TLBT7EYEND');
	</script>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '412707632792439');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=412707632792439&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</head>

<body data-spy="scroll" data-target=".nav">

	<div class="fb-customerchat" page_id="1397690197146515" minimized="true">
	</div>
	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function() {
			FB.init({
			appId            : '985230481604246',
			autoLogAppEvents : true,
			xfbml            : true,
			version          : 'v3.3'
			});
		};

		(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v3.3&autoLogAppEvents=1";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>
	
	<!-- Top Bar -->
	<?php require('topbar.php')?>
	<!-- End of Top Bar -->
	<!-- What is kthlabel -->
	<section id="product-list">
		<div class="container">	
			<span class="paragraph">
				<h1>
					งานป้ายเดือน กันยายน
				</h1>
				<div class="row">
					<p>
						งานป้ายเดือน กันยายน</br>
						ขอบคุณลูกค้าทุกท่านค่ะ 😘</br>
						</br>
						รับพิมพ์ป้าย ป้ายปกคอเสื้อ ป้าย label</br>
						🧥 ป้ายเสื้อ ป้ายกางเกง</br>
						👜 ป้ายกระเป๋า</br>
						🛌 ป้ายหมอน</br>
						🐻 ป้ายตุ๊กตา</br>
						</br></br>
						มีผ้าให้เลือก 4 ชนิด</br>
						- ผ้าซาติน ขาว ครีม เทา ดำ</br>
						- ผ้าซาตินขาว อย่างหนา มีขอบ</br>
						- ผ้าคอตตอนครีม (ผ้าดิบ)</br>
						- ผ้าคอตตอนขาว</br>
						- ผ้าซาตินสีมีขอบ (ทุกสี)</br>
					</p>
				</div>
				<div class="row row-image">
					<img class="col-xs-12 col-sm-8 " src="./assets/images/articles/70954838_2384777568437768_4390676295684456448_n.jpg" alt="ริบบิ้นผ้าดิบ">
					<img class="col-xs-12 col-sm-4" src="./assets/images/articles/71139740_2384777605104431_8894453850665123840_o.jpg" alt="ริบบิ้นผ้าดิบ">
					<img class="col-xs-12 col-sm-4" src="./assets/images/articles/70986100_2384777675104424_7304810862991114240_o.jpg" alt="ริบบิ้นผ้าดิบ">
					<img class="col-xs-12 col-sm-4" src="./assets/images/articles/70638233_2384777581771100_6824553147575304192_o.jpg" alt="ริบบิ้นผ้าดิบ">
				</div>
			</span>
		</div>
	</section>
	
	<?php include('footer.php')?>

	<script src="assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.bxslider.min.js"></script>

	<link rel="stylesheet" href="assets/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/jquery.fancybox.pack.js?v=2.1.5"></script>

	<!-- Optionally add helpers - button, thumbnail and/or media -->
	<link rel="stylesheet" href="assets/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<link rel="stylesheet" href="assets/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

	<script type="text/javascript">
	

	$(document).ready(function() {
		$('.bxslider').bxSlider({
			mode: 'fade'
		});
		$('body').scrollspy({ target: '.navbar-collapse' })
		$('.fancybox-buttons').fancybox({
			openEffect  : 'elastic',
			closeEffect : 'elastic',

			prevEffect : 'none',
			nextEffect : 'none',

			closeBtn  : false,

			helpers : {
				title : {
					type : 'inside'
				},
				buttons	: {}
			},

			afterLoad : function() {
				this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
			}
		});
	});
	</script>

	<script type='text/javascript'>
	window.__lo_site_id = 67913;
	(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
	})();
	</script>
	<script>
		(function(h,e,a,t,m,p) {
		m=e.createElement(a);m.async=!0;m.src=t;
		p=e.getElementsByTagName(a)[0];p.parentNode.insertBefore(m,p);
		})(window,document,'script','https://u.heatmap.it/log.js');
	</script>
	</body>
	</html>