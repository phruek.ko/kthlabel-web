<?php require_once 'preload.php'?>
<?php 
	$strJsonFileContents = file_get_contents("products.json");
	$productArray = json_decode($strJsonFileContents, true);
	shuffle($productArray);

	$strJsonFileContentsArticles = file_get_contents("articles.json");
	$articleArray = json_decode($strJsonFileContentsArticles, true);
	$articleArray = array_reverse($articleArray, true);


	$default_language = 'th';
	$allowed_languages = ['th', 'en'];
	
	if (isset($_GET['lang']) && in_array($_GET['lang'], $allowed_languages)) {
		$_SESSION['lang'] = $_GET['lang'];
	} else if (!isset($_SESSION['lang'])) {
		$_SESSION['lang'] = $default_language;
	}
	
	$lang = $_SESSION['lang'];

	$translations = include "./assets/languages/{$lang}.php";
?>
<!DOCTYPE html>
<html lang="th">
	<head>
		<title><?php echo $translations['tag-title'];?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="Keywords" content="ริบบิ้นพิมพ์,สกรีนริบบิ้น,ริบบิ้นพิมพ์โลโก้,ป้ายตราเสื้อ,พิมพ์ริบบิ้น,ริบบิ้นพิมพ์ลาย,พิมพ์ตราเสื้อ,ริบบิ้นผูกของขวัญ,พิมพ์โลโก้">
		<meta name="Description" content="รับพิมพ์ริบบิ้น,สกรีนริบบิ้น,ริบบิ้นพิมพ์,ริบบิ้นผ้า,โบว์พิมพ์ริบบิ้น,ป้ายทอตราเสื้อ โบว์ผูกของขวัญ">
  		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="stats-in-th" content="f55e">
		<meta name="languege" content="Thai">
		<meta name="distribution" content="Global">
		<meta name="rating" content="General">
		<meta name="area" content="Creating">
		<meta name="resource-type" content="Document">
		<meta name="revisit-after" content="1 Days">
		<meta name="placename" content="Thailand">
		<meta name="expires" content="none">
		<meta http-equiv="cache-control" content="max-age=31557600" />
		<link rel="stylesheet" href="assets/css/mini-bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/main.css">

		<!-- <link rel="stylesheet" href="assets/css/mini-bootstrap.min.css"> -->
  		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
  		<link rel="stylesheet" href="assets/css/jquery.bxslider.css">
		<!-- <link rel="stylesheet" href="assets/css/main.css"> -->
		<?php 
			if ( $detect->isMobile() ) {
				echo '<link rel="stylesheet" href="assets/css/mobile.css">';
			}
		?>
		<link rel="shortcut icon" href="assets/images/logo-black.ico">
		<!-- <link rel="stylesheet" href="node_modules/aos/dist/aos.css" /> -->
		<link rel="stylesheet" href="node_modules/aos/dist/aos.css"
							   media="none"
							   onload="if(media!='all')media='all'">
		<noscript>
			<link rel="stylesheet" href="node_modules/aos/dist/aos.css">
		</noscript>

		<script src="node_modules/aos/dist/aos.js" defer></script>

		<!-- Google tag (gtag.js) -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-TLBT7EYEND"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-TLBT7EYEND');
		</script>
		
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-WSDW9BH');</script>
		<!-- End Google Tag Manager -->
	</head>

	<body data-spy="scroll" data-target=".nav">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WSDW9BH"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
		<div class="fb-customerchat" page_id="1397690197146515" minimized="true">
		</div>
		<div id="fb-root"></div>
		<script async>
		window.fbAsyncInit = function() {
			FB.init({
			appId            : '985230481604246',
			autoLogAppEvents : true,
			xfbml            : true,
			version          : 'v3.3'
			});
		};

		(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v3.3&autoLogAppEvents=1";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>
	
		<!-- Top Bar -->
		<?php require_once('topbar.php') ?>
		<!-- End of Top Bar -->
		<!-- What is kthlabel -->
		<div id="head_info" class="hide">
			<div class="container">
				<div class="block_topic"  >
					<h1>What is KTH Label</h1>
				</div>
				<div class="block_row text_detail">
					KTH Label(บริษัท เคทีเอช เลเบิล จำกัด) คือผู้ผลิตริบบิ้นพิมพ์ลายและสกรีนริบบิ้น ด้วยเครื่องพิมพ์ระบบเลตเตอร์เพรสและเครื่องสกรีนริบบิ้น ควบคุมการทำงาน  โดยทีมงานผู้ชำนาญการพิมพ์และมีประสบการณ์ทางด้านการพิมพ์บนริบบิ้น  มามากกว่า 10 ปี ควบคุมการผลิต ด้วยคุณภาพ และความมาตราฐาน ทางร้าน  ได้พัฒนาด้านงานพิมพ์บนริบบิ้นตลอดเวลา เพื่อบริการงานพิมพ์แบบต่างๆ   ที่ลูกค้าต้องการได้หลากหลาย
				</div>
			</div>
		</div>
		

		<!-- News & Events -->
		<section id="news_events">
			<div class="container">
				<div class="block_topic">
					<h1><?php echo $translations['KTH & our services'];?></h1>
				</div>
				<div class="block_topic_desc" data-aos="fade-up" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
					<?php echo $translations['head-title'];?>
				</div>

				<div class="block_row">
					
					<div class="element col-md-4 col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
							<div class="box brd_rad">
								<div class="img">
									<a href="./product.php?category=Grosgrain">
										<?php if ( $detect->isMobile() ) {?>
											<img data-src="assets/images/products/67201677_2336046933310832_1281380201153953792_o.jpg" class="lazy brd_rad_top" alt="ริบบิ้นสกรีน">
										<?php }else{?>
											<img src="assets/images/products/67201677_2336046933310832_1281380201153953792_o.jpg" class="brd_rad_top" alt="ริบบิ้นสกรีน">
										<?php }?>
									</a>
								</div>
								<div class="topic">
									<h1><?php echo $translations['ribbon-screen'];?></h1>
								</div>
								<div class="detail">
									<?php echo $translations['ribbon-screen-description'];?>
								</div>
							</div>
						
					</div>
					<div class="element col-md-4 col-xs-12" data-aos="fade-up" data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
						<div class="box brd_rad">
							<div class="img">
								<a href="./product.php?category=Border_satin">
									<?php if ( $detect->isMobile() ) {?>
										<img data-src="assets/images/mobile/51677675_2238858536363006_5124094325717729280_n.jpg" class="lazy brd_rad_top" alt="ริบบิ้นผูกของขวัญ">
									<?php }else{?>
										<img src="assets/images/products/51677675_2238858536363006_5124094325717729280_n.jpg" class="brd_rad_top" alt="ริบบิ้นผูกของขวัญ">
									<?php }?>
								</a>
							</div>
							<div class="topic">
								<h1><?php echo $translations['present-ribbon'];?></h1>
							</div>
							<div class="detail">
								<?php echo $translations['present-ribbon-description'];?>
							</div>
						</div>
					</div>
					<div class="element col-md-4 col-xs-12" data-aos="fade-left"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
						<div class="box brd_rad">
							<div class="img">
								<a href="./product.php?category=Border_satin">
									<?php if ( $detect->isMobile() ) {?>
										<img data-src="assets/images/mobile/50128973_2223546217894238_3861856192506626048_n.jpg" class="lazy brd_rad_top" alt="ริบบิ้นเคเงิน เคทอง">
									<?php }else{ ?>
										<img src="assets/images/products/50128973_2223546217894238_3861856192506626048_n.jpg" class="brd_rad_top" alt="ริบบิ้นเคเงิน เคทอง">
									<?php }?>
								</a>
							</div>

							<div class="topic">
								<h1>
									<?php echo $translations['stamped-ribbon'];?>
								</h1>
							</div>
							<div class="detail">
								<?php echo $translations['stamped-ribbon-description'];?>
								<br/><br/><br/>
							</div>
						</div>
					</div>
				</div>

				<div class="more_news" data-aos="fade-up" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
					<a href="product.php" class="btn">Find more products</a>
				</div>	
			</div>
		</section>
		<!-- End of News & Events -->
		
		<!-- Highlight -->
		<section id="highlight">
			<div class="container">
				<div class="block_topic" data-aos="fade-up" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
					<h1>Highlight</h1>
				</div>
				<div class="block_topic_desc" data-aos="fade-up" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
					<?php echo $translations['idea and style from our customers'];?>
				</div>
				<div class="content row">
					<?php foreach($articleArray as $key => $value):
							if($value['status']=='active') {
						?>
						<div class="col-2 col-sm-4" data-aos="fade-up" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
							<div class="box brd_rad">
								<a href="<?php echo $value['link']?>" alt="<?php echo $translations == 'th' ? $value['title'] : $value['title-en']?>">
									<div class="img full-background" style="background-image:url('<?php echo $value['thumbnail-url']?>')";>
										<img src="assets/images/base-square.png" class="brd_rad_top"/>
									</div>
								</a>
								<div class="topic">
									<?php echo $lang == 'th' ? $value['title'] : $value['title-en']?>
								</div>
								<div class="detail hide">
									
								</div>
							</div>
						</div>
					<?php
						}
					 endforeach?>
				</div>
			</div>
		</section>
		<!-- End highlight -->

		<!-- Start Example product -->
		<section id="product-section">
			<div class="container">
				<div class="block_topic" data-aos="fade-up" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic" >
					<h2>Our products</h2>
				</div>
				<div class="block_topic_desc" data-aos="fade-up" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
					<?php echo $translations['product-subtitle'];?>
				</div>
				<div class="content row">
					<?php if ( $detect->isMobile() ) { $productNumber = 11;}else{$productNumber=20;}?>
					<?php for($i=0;$i<=$productNumber;$i++){$product = $productArray[$i]?>
						<a href="/product.php">
							<div class="col-12 col-sm-6 col-md-4">
								<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
									<div class="card-image" data-src="<?php echo $product['ImageUrl']?>" style="background-image:url('<?php echo $product['ImageUrl']?>')">
										<img class="lazy" src="./assets/images/image-background.png" alt="plain-image"/>
									</div>
									<div class="card-name">
										<h4><?php echo $product['ProductNameTH']?><h4>
									</div>
								</div>
							</div>
						</a>
					<?php }?>
				</div>
				<div class="footer">
					<a href="/product.php">เพิ่มเติม</a>
				</div>
			</div>
		</section>
		<!-- End Example product -->

		<!-- Main Info -->
		<section id="main_info">
			<div class="container">
				<div class="block_topic" data-aos="fade-up" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic" data-aos-anchor="#second-topic">
					<h2>Why us?</h2>
				</div>
				<div id="second-topic" class="block_topic_desc" data-aos="fade-up" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
					<?php echo $translations['why-us-subtitle'];?>
				</div>

				<div class="content">
					<div class="row1 col-md-10 col-xs-12 col-md-offset-1">
						<div class="img col-md-4 col-xs-12"  data-aos="fade-zoom-in" data-aos-delay="600"  data-aos-duration="600" data-aos-easing="ease-in-out-cubic" >
							<img data-src="assets/images/info/info1.jpg" class="lazy" alt="เนื้อผ้าริบบิ้นที่มาจากผู้ผลิตฝีมือดี">
							<div class="mask"></div>
						</div>

						<div class="info col-md-8 col-xs-12" data-aos="fade-left" data-aos-delay="800"  data-aos-duration="600" data-aos-easing="ease-in-out-cubic">
							<div class="topic">
								<?php echo $translations['why-us-sub-1-topic'];?>
							</div>
							<div class="detail">
								<?php echo $translations['why-us-sub-1-detail'];?>
							</div>
							<div class="read_more hide">
								<a href="#" class="btn">Read more</a>
							</div>
						</div>
					</div>

					<div class="row2 col-md-10 col-xs-12 col-md-offset-1">
						<div class="img col-md-4 col-xs-12 col-md-push-8" data-aos="fade-zoom-in" data-aos-delay="600"  data-aos-duration="600" data-aos-easing="ease-in-out-cubic">
							<img data-src="assets/images/info/info2.jpg" class="lazy" alt="คุณภาพของเคทองและเคเงิน">
							<div class="mask"></div>
						</div>
						<div class="info col-md-8 col-xs-12 col-md-pull-4" data-aos="fade-right" data-aos-delay="800"  data-aos-duration="600" data-aos-easing="ease-in-out-cubic">
							<div class="topic">
								<?php echo $translations['why-us-sub-2-topic'];?>
							</div>
							<div class="detail">
								<?php echo $translations['why-us-sub-2-detail'];?>
							</div>
							<div class="read_more hide">
								<a href="#" class="btn">Read more</a>
							</div>
						</div>
					</div>

					<div class="row1 col-md-10 col-xs-12 col-md-offset-1">
						<div class="img col-md-4 col-xs-12" data-aos="fade-zoom-in" data-aos-delay="800"  data-aos-duration="600" data-aos-easing="ease-in-out-cubic">
							<img data-src="assets/images/info/info3.jpg" class="lazy" alt="ความรวดเร็วในการผลิต">
							<div class="mask"></div>
						</div>

						<div class="info col-md-8 col-xs-12" data-aos="fade-left" data-aos-delay="600"  data-aos-duration="600" data-aos-easing="ease-in-out-cubic">
							<div class="topic">
								<?php echo $translations['why-us-sub-3-topic'];?>
							</div>
							<div class="detail">
								<?php echo $translations['why-us-sub-3-detail'];?>
							</div>
							<div class="read_more hide">
								<a href="#" class="btn">Read more</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End of Main Info -->

		<?php include('footer.php')?>
		
		<script src="assets/js/jquery-1.11.1.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="./assets/source/jquery.lazy-master/jquery.lazy.min.js"></script>
		<script type='text/javascript'>
			$(function(){
				$('.bxslider').bxSlider({
					mode: 'fade',
					auto: true
				});
				$('body').scrollspy({ target: '.navbar-collapse' })
				AOS.init();
			});
			
			window.__lo_site_id = 67913;
			(function() {
				var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
				wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
			  })();
			  $('.lazy').Lazy();
		</script>

		<!-- Facebook Pixel Code -->
		<script defer>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '412707632792439');
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=412707632792439&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->
		
	</body>
	 
</html>