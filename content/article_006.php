<?php require_once 'preload.php'?>
<!DOCTYPE html>
<html>
<head>
	<title>KTHLabel รับพิมพ์ริบบิ้น ป้ายตราเสื้อ ริบบิ้นพิมพ์ ริบบิ้นผูกของขวัญ ริบบิ้นผูกผม</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="Keywords" content="ริบบิ้นผูกผม, รับพิมพ์ริบบิ้น,ป้ายตราเสื้อ,ริบบิ้นพิมพ์,พิมพ์ริบบิ้น,พิมพ์ตราเสื้อ,ริบบิ้นผูกของขวัญ,พิมพ์โลโก้">
	<meta name="Description" content="รับพิมพ์ริบบิ้น,ริบบิ้นพิมพ์,ริบบิ้นผ้า,โบว์พิมพ์ริบบิ้น,ป้ายทอตราเสื้อ โบว์ผูกของขวัญ">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="stats-in-th" content="f55e">
	<meta name="languege" content="Thai">
	<meta name="distribution" content="Global">
	<meta name="rating" content="General">
	<meta name="area" content="Creating">
	<meta name="resource-type" content="Document">
	<meta name="revisit-after" content="1 Days">
	<meta name="placename" content="Thailand">
	<meta name="expires" content="none">
	<meta http-equiv="cache-control" content="max-age=31557600" />
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/jquery.bxslider.css">
	<link rel="stylesheet" href="assets/css/main.css">
	<?php 
		if ( $detect->isMobile() ) {
			echo '<link rel="stylesheet" href="assets/css/mobile.css">';
		}
	?>
	<link rel="stylesheet" href="assets/css/product.css">
	<link rel="shortcut icon" href="assets/images/logo-black.ico">
	<!-- <script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-25373992-1', 'auto');
	ga('send', 'pageview');

	</script> -->
	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-TLBT7EYEND"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-TLBT7EYEND');
	</script>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '412707632792439');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=412707632792439&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</head>

<body data-spy="scroll" data-target=".nav">

	<div class="fb-customerchat" page_id="1397690197146515" minimized="true">
	</div>
	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function() {
			FB.init({
			appId            : '985230481604246',
			autoLogAppEvents : true,
			xfbml            : true,
			version          : 'v3.3'
			});
		};

		(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v3.3&autoLogAppEvents=1";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>
	
	<!-- Top Bar -->
	<?php require('topbar.php')?>
	<!-- End of Top Bar -->
	<!-- What is kthlabel -->
	<section id="product-list">
		<div class="container">	
			<span class="paragraph">
				
				<h1 class="base_text">
					ริบบิ้นผูกผมจากลูกค้า KTH
				</h1>
				<p class="base_text">
					KTHLabel รับผลิตริบบิ้นผูกผมคัสต้อมโลโก้ตามที่ลูกค้าต้องการ โลโก้โรงเรียนหรือโลโก้ต่างๆที่ลูกค้าตามแต่ลูกค้าต้องการ พร้อมขึ้นรูปโบว์สำเร็จพร้อมกิ๊ฟท์พร้อมใช้งาน
				</p>
				<div class="row">
					<div class="element col-xs-12 no-padding" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
						<div class="col-sm-6 no-padding">
							<img class="col-xs-12" src="assets/images/articles/347826567_584554887114829_7180215489915049750_n.jpeg" />
						</div>
						<div class="col-sm-6 no-padding">
							<div class="col-xs-12">
								<p class="base_text">
									ริบบิ้นซาตินมีขอบ สกรีนชื่อ / โบว์ผูกผม 🍊🧡</br>
									สกรีนสีขาว หรือสกรีน 1 สี</br>
									🧻 ความยาวต่อม้วน 33 หลา</br>
									😚 สั่งขั้นต่ำ 10 ม้วน</br>
									🎗 มีขนาดให้เลือก 1, 1.3, 2, 2.5, 3.8 cm	</br>
								</p>
							</div>

							<img class="col-xs-12"  src="assets/images/articles/347811295_581208937325405_6815152044613457535_n.jpeg" alt="ริบบิ้นผูกผม"/>
						
						</div>
					</div>
					<hr class="row">
					
					<div class="element col-xs-12 no-padding" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
						<div class="col-sm-6 no-padding">
							<img class="col-xs-12"  src="assets/images/articles/S__64741435.jpg" alt="ริบบิ้นผูกผม"/>
							<div class="col-xs-12">
								<p>
								ริบบิ้นผูกผมสำเร็จรูปจาก KTHLabel
								</p>
							</div>
							<img class="col-xs-12"  src="assets/images/articles/S__64741436.jpg" alt="ริบบิ้นผูกผม"/>
						</div>
						<div class="col-sm-6">
							<img class="col-xs-12"  src="assets/images/articles/339658211_960665568640873_443723641817379933_n.jpeg" alt="ริบบิ้นผูกผม"/>
							<div class="col-xs-12">
								<p class="base_text">
								ริบบิ้นซาตินมีขอบ สกรีนชื่อโรงเรียน โบว์ผูกผมนักเรียน</br>
								สกรีนสีขาว หรือสกรีน 1 สี</br>
								🧻 ความยาวต่อม้วน 33 หลา</br>
								😚 สั่งขั้นต่ำ 10 ม้วน</br>
								🎗 มีขนาดให้เลือก 1, 1.3, 2, 2.5, 3.8 cm</br>
								รับทำโบว์สำเร็จติดกิ้บ ขั้นต่ำ 100 ชิ้นขึ้นไป</br>

								</p>
							</div>
						</div>
					</div>

					<hr class="row">

					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
						<div class="col-sm-6">
							<img class="col-xs-12"  src="assets/images/articles/S__64741434.jpg" alt="ริบบิ้นผูกผม"/>	
					
						</div>

						<div class="col-sm-6">
							<img class="col-xs-12"  src="assets/images/articles/S__64741431.jpg" alt="ริบบิ้นผูกผม"/>	
							
						</div>

					</div>

					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
						<div class="col-sm-6">
							<div class="img full-background">
								<img class="col-xs-12"  src="assets/images/articles/S__64913464.jpg" alt="ริบบิ้นผูกผม"/>	
							</div>
						</div>

						<div class="col-sm-6">
							<div class="img full-background">
								<img class="col-xs-12"  src="assets/images/articles/S__64913465.jpg" alt="ริบบิ้นผูกผม"/>	
							</div>
						</div>

					</div>

					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
						<div class="col-sm-6">
							<div class="img full-background">
								<img class="col-xs-12"  src="assets/images/products/67343049_2336050899977102_6890107551241732096_n.jpg" alt="ริบบิ้นผูกผม"/>	
							</div>
						</div>

						<div class="col-sm-6">
							<div class="img full-background">
								<img class="col-xs-12"  src="assets/images/products/66958602_2336050919977100_2805870149855346688_n.jpg" alt="ริบบิ้นผูกผม"/>	
							</div>
						</div>
						
					</div>

					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
						<div class="col-sm-6">
							<div class="img full-background">
								<img class="col-xs-12"  src="assets/images/products/60337510_2292418614340331_5854116926405476352_o.jpg" alt="ริบบิ้นผูกผม"/>	
								<img class="col-xs-12"  src="assets/images/articles/138805963_2785258241723030_5184061164472646662_n.jpeg" alt="ริบบิ้นผูกผม"/>
								
							</div>
						</div>

						<div class="col-sm-6">
							<div class="img full-background">
								<img class="col-xs-12"  src="assets/images/articles/137667828_2785258268389694_3881509108036349085_n.jpeg" alt="ริบบิ้นผูกผม"/>	
							</div>
						</div>
						
					</div>
				</div>
			</span>
		</div>
	</section>
	
	<?php include('footer.php')?>

	<script src="assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.bxslider.min.js"></script>

	<link rel="stylesheet" href="assets/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/jquery.fancybox.pack.js?v=2.1.5"></script>

	<!-- Optionally add helpers - button, thumbnail and/or media -->
	<link rel="stylesheet" href="assets/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<link rel="stylesheet" href="assets/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

	<script type="text/javascript">
	

	$(document).ready(function() {
		$('.bxslider').bxSlider({
			mode: 'fade'
		});
		$('body').scrollspy({ target: '.navbar-collapse' })
		$('.fancybox-buttons').fancybox({
			openEffect  : 'elastic',
			closeEffect : 'elastic',

			prevEffect : 'none',
			nextEffect : 'none',

			closeBtn  : false,

			helpers : {
				title : {
					type : 'inside'
				},
				buttons	: {}
			},

			afterLoad : function() {
				this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
			}
		});
	});
	</script>

	<script type='text/javascript'>
	window.__lo_site_id = 67913;
	(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
	})();
	</script>
	<script>
		(function(h,e,a,t,m,p) {
		m=e.createElement(a);m.async=!0;m.src=t;
		p=e.getElementsByTagName(a)[0];p.parentNode.insertBefore(m,p);
		})(window,document,'script','https://u.heatmap.it/log.js');
	</script>
	</body>
	</html>