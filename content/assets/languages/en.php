<?php
return [
    'tag-title' => 'KTHLabel Ribbon Printing, Screen Printing Ribbon, Logo Printing Ribbon, Shirt Label, Printed Ribbon, Gift Wrapping Ribbon',
    'welcome' => 'welcome',
    'KTH & our services' => 'KTH & Our services',
    'head-title' => 'KTH Label is a provider of ribbon printing. Leveraging letterpress and screen printing technologies, our highly skilled and experienced team, with over a decade of industry experience, ensures the highest quality and standards in our products. We are constantly innovating and developing our ribbon printing techniques to cater to a wide array of customer needs in the most diverse ways.',
    'ribbon-screen' => 'Screen Printed Ribbon',
    'ribbon-screen-description' => 'Choose the color, logo, and various fabrics for your screen printed ribbon. It can be used in various ways, such as gift wrapping or as a necklace strap.',
    'present-ribbon' => 'Gift Wrapping Ribbon',
    'present-ribbon-description' => 'Add your personal touch to the ribbon with your printed logo. Enhance its prominence and create a distinction for your product',
    'stamped-ribbon' => 'Foil-Stamped Ribbon',
    'stamped-ribbon-description' => 'Silver and gold foil-stamped ribbons add a premium touch to your product with reflective logos or letters',
    'kthlabel-company' => 'CO.,LTD',
    'idea and style from our customers'=>'Products and ideas from KTH and our customers',
    'product-subtitle' => 'KTH offers a wide variety of products, and these are examples of our products. You can view more examples in our product menu',
    'why-us-subtitle' => 'Here at KTH, we are passionate experts in ribbon printing. We do not just create beautiful works of art, we are also here to help guide you in choosing the perfect fabric that suits your individual needs and style',
    
    'why-us-sub-1-topic' => 'Our ribbon fabric is crafted with care and expertise from the selected providers',
    'why-us-sub-1-detail' => 'We carefully select top-quality manufacturers specifically for your needs. We do this so your customers do not feel discomfort when wearing clothes with your shirt logo, and we choose materials perfect for all kinds of beautiful gift wrapping ribbons',

    'why-us-sub-2-topic' => 'The quality of gold and silver foil',
    'why-us-sub-2-detail' => 'At our production, we do not just use any gold and silver foils. We choose only high-quality, beautiful ones that come straight from our trusted partners. This means you can count on your gold and silver hues to stay vibrant and intact for a longer time, without worrying about them fading or peeling off easily',

    'why-us-sub-3-topic' => 'We pride ourselves on our speedy production process',
    'why-us-sub-3-detail' => 'Thanks to our expertise at KTH Label, we are able to produce at a rapid pace. But it is not just about speed - we also guarantee that you are getting a product you can trust for its quality, delivered in no time',
];

?>