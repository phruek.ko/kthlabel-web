<?php require_once 'preload.php'?>
<!DOCTYPE html>
<html>
<head>
	<title>KTHLabel รับพิมพ์ริบบิ้น ป้ายตราเสื้อ ริบบิ้นพิมพ์ ริบบิ้นผูกของขวัญ </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="Keywords" content="รับพิมพ์ริบบิ้น,ป้ายตราเสื้อ,ริบบิ้นพิมพ์	,พิมพ์ริบบิ้น,พิมพ์ตราเสื้อ,ริบบิ้นผูกของขวัญ,พิมพ์โลโก้">
	<meta name="Description" content="รับพิมพ์ริบบิ้น,ริบบิ้นพิมพ์,ริบบิ้นผ้า,โบว์พิมพ์ริบบิ้น,ป้ายทอตราเสื้อ โบว์ผูกของขวัญ">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="stats-in-th" content="f55e">
	<meta name="languege" content="Thai">
	<meta name="distribution" content="Global">
	<meta name="rating" content="General">
	<meta name="area" content="Creating">
	<meta name="resource-type" content="Document">
	<meta name="revisit-after" content="1 Days">
	<meta name="placename" content="Thailand">
	<meta name="expires" content="none">
	<meta http-equiv="cache-control" content="max-age=31557600" />
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/jquery.bxslider.css">
	<link rel="stylesheet" href="assets/css/main.css">
	<?php 
		if ( $detect->isMobile() ) {
			echo '<link rel="stylesheet" href="assets/css/mobile.css">';
		}
	?>
	<link rel="stylesheet" href="assets/css/product.css">
	<link rel="shortcut icon" href="assets/images/logo-black.ico">
	<!-- <script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-25373992-1', 'auto');
	ga('send', 'pageview');

	</script> -->
	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-TLBT7EYEND"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-TLBT7EYEND');
	</script>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '412707632792439');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=412707632792439&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</head>

<body data-spy="scroll" data-target=".nav">

	<div class="fb-customerchat" page_id="1397690197146515" minimized="true">
	</div>
	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function() {
			FB.init({
			appId            : '985230481604246',
			autoLogAppEvents : true,
			xfbml            : true,
			version          : 'v3.3'
			});
		};

		(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v3.3&autoLogAppEvents=1";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>
	
	<!-- Top Bar -->
	<?php require('topbar.php')?>
	<!-- End of Top Bar -->
	<!-- What is kthlabel -->
	<section id="product-list">
		<div class="container">	
			<span class="paragraph">
				
				<h1>
					ไอเดียการใช้ริบบิ้นบนสินค้าประเภทขนม จากลูกค้า KTH
				</h1>
				<p>
					ริบบิ้นของทางลูกค้า KTH ถูกประยุกต์ใช้กับสินค้าหลากหลายประเภท จนล่าสุด กล่องโดนัทพรีเมียม ของฝากสำหรับลูกค้าไฮเอน ก็ยังเลือกใช้ริบบิ้นคุณภาพดีจากทาง KTH
				</p>
				<div class="row">
					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">

						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
							
									<img class="col-xs-12 no-padding" src="assets/images/articles/240623174_2951892515059601_8207817145823264790_n.jpeg" />
								</div>
								<div class="card-name">
									<p>
									ริบบิ้นกรอสเกรน 🎀 พิมพ์ชื่อร้าน ชื่อแบรนด์ ข้อความตามสั่ง
									ขนาด 1.5 Cm สกรีน 1 สี
									ความยาวต่อม้วน 33 หลา
									</p>
								</div>
							</div>
						</div>

						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
								
									<img class="col-xs-12 no-padding"  src="assets/images/articles/241688153_2951890561726463_6707897264068623587_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>
								</div>
								<div class="card-name">
									<p alt="ริบบิ้นผ้าต่วน">
									ริบบิ้นซาตินสีขาว 🎀 พิมพ์ชื่อร้าน ชื่อแบรนด์ ข้อความตามสั่ง
									🧻 ความยาวต่อม้วน 200 หลา
									🎗 มีขนาดให้เลือก 1 -6 cm
									</p>
								</div>
							</div>
						</div>
					</div>
					
					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">

						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/104433340_2612105929038263_5763811298118242215_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>
								</div>
								<div class="card-name">
								<p>
								ภาพรีวิว จาก ร้านHomlaor จ.สุพรรณบุรี
								ริบบิ้นผูกกล่องเค้ก ขนม คุกกี้ เบเกอรี่ต่างๆ 🍰🥧🍪🍩
								ริบบิ้นผ้าต่วน ปั้มฟอยล์ทอง  🎀พิมพ์ชื่อร้าน 
								🧻 ความยาวต่อม้วน 200 หลา
								🎗 มีขนาดให้เลือก 1, 1.2, 1.5, 2.1, 3, 4 cm
								</p>
								</div>
							</div>
						</div>


						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/products/286782446_3153089701606547_115133165819336939_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
								<p>
								ริบบิ้นกรอสเกรน สกรีน 1 สี
								🧻 ความยาวต่อม้วน 33 หลา / 50 หลา
								🎗 มีขนาดให้เลือก 1, 1.5, 2.5 ซม
								</p>
								</div>
							</div>
						</div>

					</div>

					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">

						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/347811922_1539148973560601_5054255540295554148_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
									<p>
									ขอบคุณ 💝 ภาพจากร้าน งามศิลป์ ขนมไทย
									
									ริบบิ้นซาตินมีขอบ พิมพ์ชื่อร้าน ชื่อแบรนด์
									ข้อความตามสั่ง
									
									พิมพ์ 1 สี ปั้มฟอยล์สีเงิน หรือ ทอง
									🧻 ความยาวต่อม้วน 33 หลา
									🎗 มีขนาดให้เลือก 1, 1.3, 2, 2.5, 3.8 cm
									</p>
								</div>
							</div>
						</div>


						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/products/286648129_3153097794939071_3911008837623290037_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
								<p>
								Durian Lover 😍😍😍
								ซีซั่นนี้ต้องกินทุเรียนเท่านั้น 
								ริบบิ้นผูกกล่องทุเรียน ผูกกล่องผลไม้ กระเช้าผลไม้ 🎀🎁
								ริบบิ้นผ้าต่วน พิมพ์ชื่อร้าน ชื่อแบรนด์ ข้อความตามสั่ง
								ปั้มฟอยล์สีเงิน หรือทอง
								🧻 ความยาวต่อม้วน 200 หลา
								🎗มีขนาด 1, 1.2, 1.5, 2.1, 3, 4, 5, 6 cm
								</p>
								</div>
							</div>
						</div>
					</div>

					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
					
						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/347803544_562253532480247_4217787001899933419_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
								<p>
									ขอบคุณ ภาพจากร้าน ivan factory 💝
								
									ริบบิ้นซาตินมีขอบ อย่างหนา สีขาว
									พิมพ์ชื่อร้าน ชื่อแบรนด์ ข้อความตามสั่ง
									
									พิมพ์ 1 -4 สี / ปั้มฟอยล์สีเงิน หรือ ทอง
									🧻 ความยาวต่อม้วน 200 เมตร
									🎗 มีขนาดให้เลือก 1-6 ซม.
								</p>
								</div>
							</div>
						</div>

						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/339994618_231816722714594_441364064192027392_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
								<p>
									โบว์สำเร็จ 🎀
									ริบบิ้นซาตินขาว ขนาด 2.5 ซม พิมพ์ชื่อร้าน ชื่อแบรนด์ พิมพ์ข้อความตามสั่ง ซ้อนบนผ้าต่วนสีขนาด 3 ซม
								</p>
								</div>
							</div>
						</div>

					</div>
					
					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/329537249_1921348911544160_7235764593914119448_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
								<p>
									ขอบคุณ 💝 ภาพจากร้าน งามศิลป์ ขนมไทย
									พิมพ์ 1 สี ปั้มฟอยล์สีเงิน หรือ ทอง
									🧻 ความยาวต่อม้วน 33 หลา
								</p>
								</div>
							</div>
						</div>

						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/328252760_553217453407684_5851472929107510775_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
								<p>
									ขอบคุณ ภาพจากร้าน พรนับพัน
									พิมพ์ 1 สี ปั้มฟอยล์สีเงิน หรือ ทอง
									ผูกโบว์สองเลเยอร์
								</p>
								</div>
							</div>
						</div>
					</div>

					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/328248247_514598897484364_5279738198392044546_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
								<p>
								ขอบคุณ ❤️ ภาพสวยๆ จากร้าน Hidden Flower
								สนใจพิมพ์ริบบิ้น ผูกกระเช้าดอกไม้ 💐ช่อดอกไม้
								</p>
								</div>
							</div>
						</div>

						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/321112112_6183531384998852_2634906862314475771_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
								<p>
								ภาพจากร้าน mesook bakery
								ริบบิ้นผูกกล่องขนมสองเลเยอร์
								</p>
								</div>
							</div>
						</div>
					</div>
					
					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
					
						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/280710382_3132886190293565_1299806640701046082_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
									<p>
									ริบบิ้นคอตตอนครีม (ผ้าดิบ)/ คอตตอนขาว
									พิมพ์ชื่อร้าน ชื่อแบรนด์ โลโก้ ข้อความตามสั่ง
									</p>
								</div>
							</div>
						</div>

						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/271656605_3041077012807817_6607464088280978108_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
									<p>
									ขอบคุณ 🙏 รูปรีวิวจากลูกค้าค่ะ
									
									ริบบิ้นซาตินมีขอบ สีขาว อย่างหนา
									ขนาดกว้าง 2 ซม
									พิมพ์โลโก้สีเขียวพาสเทล
									
									ริบบิ้น พิมพ์โลโก้ ชื่อร้าน ชื่อแบรนด์ ข้อความตามสั่ง
									</p>
								</div>
							</div>
						</div>
					</div>


					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
					
						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/270017251_3032815913633927_5667987065690969578_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
									<p>
									ขอบคุณ 🙏 รูปรีวิวจากลูกค้าค่ะ
									
									ริบบิ้นซาติน สีขาว ขนาดกว้าง 2.5 ซม (1 นิ้ว )
									พิมพ์โลโก้สีดำ
									
									ริบบิ้น พิมพ์โลโก้ ชื่อร้าน ชื่อแบรนด์ ข้อความตามสั่ง
									</p>
								</div>
							</div>
						</div>

						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/269715076_3029574713958047_4662797748391525695_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
									<p>
									ขอบคุณ 🙏 รูปรีวิวจากลูกค้าค่ะ 😚🎁
									
									ริบบิ้นผ้าต่วน 🎀 พิมพ์ชื่อร้าน ชื่อแบรนด์ ข้อความตามสั่ง
									ปั้มฟอยสีเงิน หรือทอง
									🧻 ความยาวต่อม้วน 200 หลา
									🎗 มีขนาดให้เลือก 1, 1.2, 1.5, 2.1, 3, 4 ซม
									</p>
								</div>
							</div>
						</div>
					</div>

					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
					
						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/216272574_2911179452464241_6214648038507493956_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
									<p>
									ขอบคุณภาพ จากร้าน Gozzy fruit
									
									ริบบิ้นผ้าต่วนสี 🎀พิมพ์ชื่อร้าน ข้อความตามสั่ง
									ปั้มฟอย สีเงิน,ทอง,พิ้งโกล
									
									🧻 ความยาวต่อม้วน 200 หลา
									🎗 มีขนาดให้เลือก 1, 1.2, 1.5, 2.1, 3, 4 cm
									</p>
								</div>
							</div>
						</div>

						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/212227882_2904688666446653_1572274960598034698_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
									<p>
									ริบบิ้นซาตินมีขอบ 🎀พิมพ์ชื่อร้าน ข้อความตามสั่ง
									พิมพ์สี หรือปั้มฟอยล์สีเงิน / สีทอง
									
									🧻 ความยาวต่อม้วน 33 หลา
									🎗 มีขนาดให้เลือก 1, 1.3, 2, 2.5 cm
									</p>
								</div>
							</div>
						</div>
					</div>

					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
					
						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/187378886_2904687253113461_1263888851877718919_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
									<p>
									ริบบิ้นซาตินมีขอบ อย่างหนา สีขาว ขนาดกว้าง 1 ซม
									พิมพ์ 1 สี
									
									พิมพ์ชื่อร้าน ชื่อแบรนด์ ข้อความตามสั่ง
									ริบบิ้นผูกกล่องคุกกี้ กล่องขนม กล่องเค้ก เบเกอรี่ต่างๆ
									กล่องสินค้า แพคเกจ ของว่าง ของฝาก ผูกช่อดอกไม้ กระเช้าดอกไม้ ตะกร้าผลไม้ ผูกของขวัญ
									
									</p>
								</div>
							</div>
						</div>

						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/210198991_2903502346565285_3739588984693834689_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
									<p>
									ริบบิ้นซาตินมีขอบ 🎀พิมพ์ชื่อร้าน ข้อความตามสั่ง
									พิมพ์สี หรือปั้มฟอยล์สีเงิน / สีทอง
									
									🧻 ความยาวต่อม้วน 33 หลา
									</p>
								</div>
							</div>
						</div>
					</div>

					<div class="element col-xs-12" data-aos="fade-right"  data-aos-delay="600"  data-aos-duration="500" data-aos-easing="ease-in-out-cubic" data-aos-duration="300">
					
						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/208750520_2903501353232051_6430751036611094782_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
									<p>
									ริบบิ้นผ้าต่วนสี 🎀พิมพ์ชื่อร้าน ข้อความตามสั่ง
									พิมพ์ 1 สี หรือ ปั้มฟอย สีเงิน,ทอง,พิ้งโกล
									
									🧻 ความยาวต่อม้วน 200 หลา
									🎗 มีขนาดให้เลือก 1, 1.2, 1.5, 2.1, 3, 4 cm
									</p>
								</div>
							</div>
						</div>

						<div class="col-sm-6 web-theme">
							<div class="card-container" data-aos="fade" data-aos-offset="200" data-aos-delay="200"  data-aos-duration="200" data-aos-easing="ease-in-out-cubic">
								<div class="card-image">
									<img class="col-md-12 no-padding"  src="assets/images/articles/186461850_2868747886707398_2445125222586557868_n.jpeg" alt="ริบบิ้นผ้าผูกกล่องขนม"/>	
								</div>
								<div class="card-name">
									<p>
									ริบบิ้นผ้าต่วนสี 🎀พิมพ์ชื่อร้าน ข้อความตามสั่ง
									ปั้มฟอย สีเงิน,ทอง,พิ้งโกล
									🧻 ความยาวต่อม้วน 200 หลา
									🎗 มีขนาดให้เลือก 1-6 cm
									</p>
								</div>
							</div>
						</div>
					</div>


				</div>
			</span>
		</div>
	</section>
	
	<?php include('footer.php')?>

	<script src="assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.bxslider.min.js"></script>

	<link rel="stylesheet" href="assets/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/jquery.fancybox.pack.js?v=2.1.5"></script>

	<!-- Optionally add helpers - button, thumbnail and/or media -->
	<link rel="stylesheet" href="assets/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<link rel="stylesheet" href="assets/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

	<script type="text/javascript">
	

	$(document).ready(function() {
		$('.bxslider').bxSlider({
			mode: 'fade'
		});
		$('body').scrollspy({ target: '.navbar-collapse' })
		$('.fancybox-buttons').fancybox({
			openEffect  : 'elastic',
			closeEffect : 'elastic',

			prevEffect : 'none',
			nextEffect : 'none',

			closeBtn  : false,

			helpers : {
				title : {
					type : 'inside'
				},
				buttons	: {}
			},

			afterLoad : function() {
				this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
			}
		});
	});
	</script>

	<script type='text/javascript'>
	window.__lo_site_id = 67913;
	(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
	})();
	</script>
	<script>
		(function(h,e,a,t,m,p) {
		m=e.createElement(a);m.async=!0;m.src=t;
		p=e.getElementsByTagName(a)[0];p.parentNode.insertBefore(m,p);
		})(window,document,'script','https://u.heatmap.it/log.js');
	</script>
	</body>
	</html>