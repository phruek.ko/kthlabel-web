<!-- Footer -->
		<footer id="footer">
			<div class="container">
				<div id="sitemap" class="footer_row col-md-3">
					<div class="topic"><span class="bold">SITE</span> MAP</div>

					<ul class="list">
						<li><a href="#">What is KTH</a></li>
						<li><a href="product.php">Our Products</a></li>
						<li><a href="#">Why us</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>

				<div id="subscription" class="footer_row col-md-5 col-xs-12">
					
					<div class="fb-page" data-href="https://www.facebook.com/KTHlabel/" data-tabs="timeline" data-height="300" data-small-header="true" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/KTHlabel/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/KTHlabel/">KTH label รับพิมพ์ริบบิ้น ริบบิ้นพิมพ์ลาย ริบบิ้นผูกของขวัญ</a></blockquote></div>					
					<!-- <div class="topic col-md-8 col-md-offset-1">
						<span class="bold">SUBSCRIBE</span> NOW!</div>

					<div class="detail">
						<div id="subscription_form" action="" method="post">
							<input type="text" disabled name="email" placeholder="Enter your email here to get updated news" class="txtfield brd_rad col-md-8 col-xs-10 col-xs-offset-1"><br />
							<a href="http://www.facebook.com/KTHlabel"  class="brd_rad col-md-8 col-xs-10 col-xs-offset-1">
								<button type="submit" class="brd_rad col-md-8 col-xs-10 col-xs-offset-1">
									Subscribe
								</button>
							</a>
						</div>
					</div>-->
				</div> 

				<div class="footer_row col-md-4 col-xs-12">
					<div class="topic">KTHLABEL <?php echo $translations['kthlabel-company'];?></div>
						
						<div class="detail">
						87 Anamai-ngam-charoen Thakham Bangkhunthain<br />
						Bangkok, 10150 <br/>
						<div class="bold">Email : chaiwat_label@hotmail.com</div>
						<div class="bold">Line : 085-841-2344</div>
						<div class="space-around">
							<img src="./assets/images/kthlabel_line.png" alt="kthlabel"/>
						</div>
						<div class="bold">Tel : 02-896-5945 , 085-841-2344</div>
						<div id="icons">
							<a href="http://www.facebook.com/KTHlabel" target="_blank"><img src="assets/images/c-icon-facebook.png" alt="kthlabel/facebook"></a>
							<a href="http://www.twitter.com/" target="_blank"><img src="assets/images/c-icon-twitter.png" alt="kthlabel/twitter"></a>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- End of Footer -->