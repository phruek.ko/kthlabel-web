<div id="top_bar">
	<div class="container">
		<div class="col-xs-12 lang-bar">
			<div class="button-group">
				<a href="?lang=th" class="<?php 
					echo ($_SESSION['lang']=='th' ? 'hilight' :'')
				?>">Thai</a> |
				<a href="?lang=en" class="<?php 
					echo ($_SESSION['lang']=='en' ? 'hilight' :'')
				?>">English</a>
			</div>
		</div>
		<header class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!--navigator-->
			<div id="nav" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
				<a href="index.php" aria-label="home">
					<div class="logo" data-aos="fade-zoom-in" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="300"></div>
				</a>
				<div class="navbar-header">
					<div class="logo-mini data-aos="fade-zoom-in" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600""></div>
					<button type="button" class="navbar-toggle animate" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<i class="fa fa-ellipsis-h"></i>
					</button>
				</div>
				<nav class="navbar-collapse collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right" >
						<li><a href="index.php#news_events">KTH & our services</a></li>
						<li><a href="product.php">Products</a></li>
						<li><a href="article.php">Articles</a></li>
						<li><a href="index.php#main_info">Why us?</a></li>
						<li><a href="index.php#footer">Contact Us</a></li>
					</ul>
				</nav>
			</div>
			
			<!--end navigator-->
		</header>	
	</div>
	<div class="container-fluid" >
		<ul class="bxslider">
			<li id="banner1" data-src="./assets/images/banner1.jpg"></li>
			<li id="banner2" data-src="./assets/images/banner2.jpg"></li>
			<li id="banner3" data-src="./assets/images/banner3.jpg"></li>
			<li id="banner4" data-src="./assets/images/banner4.jpg"></li>
		</ul>
	</div>
</div>
