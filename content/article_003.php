<?php require_once 'preload.php'?>
<!DOCTYPE html>
<html>
<head>
	<title>KTHLabel รับพิมพ์ริบบิ้น ป้ายตราเสื้อ ริบบิ้นพิมพ์ ริบบิ้นผูกของขวัญ </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="Keywords" content="รับพิมพ์ริบบิ้น,ป้ายตราเสื้อ,ริบบิ้นพิมพ์	,พิมพ์ริบบิ้น,พิมพ์ตราเสื้อ,ริบบิ้นผูกของขวัญ,พิมพ์โลโก้">
	<meta name="Description" content="รับพิมพ์ริบบิ้น,ริบบิ้นพิมพ์,ริบบิ้นผ้า,โบว์พิมพ์ริบบิ้น,ป้ายทอตราเสื้อ โบว์ผูกของขวัญ">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="stats-in-th" content="f55e">
	<meta name="languege" content="Thai">
	<meta name="distribution" content="Global">
	<meta name="rating" content="General">
	<meta name="area" content="Creating">
	<meta name="resource-type" content="Document">
	<meta name="revisit-after" content="1 Days">
	<meta name="placename" content="Thailand">
	<meta name="expires" content="none">
	<meta http-equiv="cache-control" content="max-age=31557600" />
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/jquery.bxslider.css">
	<link rel="stylesheet" href="assets/css/main.css">
	<?php 
		if ( $detect->isMobile() ) {
			echo '<link rel="stylesheet" href="assets/css/mobile.css">';
		}
	?>
	<link rel="stylesheet" href="assets/css/product.css">
	<link rel="shortcut icon" href="assets/images/logo-black.ico">
	<!-- <script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-25373992-1', 'auto');
	ga('send', 'pageview');

	</script> -->
	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-TLBT7EYEND"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-TLBT7EYEND');
	</script>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '412707632792439');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=412707632792439&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</head>

<body data-spy="scroll" data-target=".nav">

	<div class="fb-customerchat" page_id="1397690197146515" minimized="true">
	</div>
	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function() {
			FB.init({
			appId            : '985230481604246',
			autoLogAppEvents : true,
			xfbml            : true,
			version          : 'v3.3'
			});
		};

		(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v3.3&autoLogAppEvents=1";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>
	
	<!-- Top Bar -->
	<?php require('topbar.php')?>
	<!-- End of Top Bar -->
	<!-- What is kthlabel -->
	<section id="product-list">
		<div class="container">	
			<span class="paragraph">
				<div class="row no-padding">
					<h1>
						ป้ายผ้าดิบมาแรงมาก!!! บอกเลย 🤩
					</h1>
				</div>
				<div class="row no-padding">
					<div class="col-xs-12 no-padding">
						ป้ายคอตตอนสีครีม (ผ้าดิบ)</br>
						พิมพ์สีออฟเซต 1-2 สี</br>
						มีขนาดกว้าง 1-4 ซม</br>
						ความยาวต่อม้วน 100 เมตร
					</div>
				</div>
				<div class="row no-padding">
					<h1>
						Linin
					</h1>
				</div>
				<div class="row row-image no-padding">
					<div class="col-xs-12 col-sm-6 no-padding">
						<img src="./assets/images/products/71299781_2390255551223303_564721395502678016_o.jpg" />
					</div>
					<div class="col-xs-12 col-sm-6 no-padding">
						<img src="./assets/images/products/o008a7fbbad1661559608a74815caf53a_49977693_191103_0015.jpg" />
					</div>
					<div class="col-xs-12 col-sm-6 no-padding">
						<img src="./assets/images/products/o008a7fbbad1661559608a74815caf53a_49977693_191103_0017.jpg" />
					</div>
					<div class="col-xs-12 col-sm-6 no-padding">
						<img src="./assets/images/products/o008a7fbbad1661559608a74815caf53a_49977693_191103_0016.jpg" />
					</div>
				</div>
				<hr/>
				<div class="row no-padding">
					<h1>
						Varni
					</h1>
				</div>
				<div class="row row-image no-padding">
					<div class="col-xs-12 col-sm-6 no-padding">
						<img src="./assets/images/products/o008a7fbbad1661559608a74815caf53a_49977693_191103_0001.jpg" />
						<img src="./assets/images/products/o008a7fbbad1661559608a74815caf53a_49977693_191103_0022.jpg" />
					</div>
					<div class="col-xs-12 col-sm-6 no-padding">
						<img src="./assets/images/products/o008a7fbbad1661559608a74815caf53a_49977693_191103_0019.jpg" />
						<img src="./assets/images/products/o008a7fbbad1661559608a74815caf53a_49977693_191103_0018.jpg" />
					</div>
				</div>
				<hr/>
				<div class="row no-padding">
					<h1>
						Rain forist
					</h1>
				</div>
				<div class="row row-image no-padding">
					<div class="col-xs-12 col-sm-4 no-padding">
						<img src="./assets/images/products/o008a7fbbad1661559608a74815caf53a_49977693_191103_0020.jpg" />
					</div>
					<div class="col-xs-12 col-sm-8 no-padding">
						<img src="./assets/images/products/52384537_2240573676191492_7750655977581445120_n.jpg" />
					</div>
				</div>

				<hr/>
				<div class="row no-padding">
					<h1>
						Rakjang
					</h1>
				</div>
				<div class="row row-image no-padding">
					<div class="col-xs-12 col-sm-6 no-padding">
						<img src="./assets/images/products/o008a7fbbad1661559608a74815caf53a_49977693_191103_0002.jpg" />
					</div>
					<div class="col-xs-12 col-sm-3 no-padding">
						<img src="./assets/images/products/o008a7fbbad1661559608a74815caf53a_49977693_191103_0014.jpg" />
					</div>
					<div class="col-xs-12 col-sm-3 no-padding">
						<img src="./assets/images/products/o008a7fbbad1661559608a74815caf53a_49977693_191103_0012.jpg" />
					</div>
				</div>
				<hr/>
				<div class="row no-padding">
					<h1>
						Others
					</h1>
				</div>
				<div class="row row-image no-padding">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/51818499_2240573672858159_9196950708014809088_n.jpg" alt="ริบบิ้นผ้าดิบ">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/articles/71059681_2390255844556607_4175472243444285440_n.jpg" alt="ริบบิ้นผ้าดิบ">
				</div>
				<div class="row row-image no-padding">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/articles/71104086_2390255871223271_628756471668015104_n.jpg" alt="ริบบิ้นผ้าดิบ">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/articles/71185035_2390255734556618_5298904455352156160_n.jpg" alt="ริบบิ้นผ้าดิบ">
				</div>

				<div class="row row-image no-padding">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/articles/71297802_2390255541223304_7072916189881565184_o.jpg" alt="ริบบิ้นผ้าดิบ">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/articles/71302245_2390255531223305_5500781859727998976_o.jpg" alt="ริบบิ้นผ้าดิบ">
				</div>
			
				<div class="row row-image">
					<div class="col-xs-12 col-sm-6 no-padding">
						<img class="col-xs-12 col-sm-12" src="./assets/images/articles/72452013_2390255614556630_2600386746772357120_n.jpg" alt="ริบบิ้นผ้าดิบ">
					</div>
					<div class="col-xs-12 col-sm-6 no-padding">
						<img class="col-xs-12 no-padding" src="./assets/images/articles/71757698_2390255684556623_7695678106975076352_n.jpg" alt="ริบบิ้นผ้าดิบ">
						<img class="col-xs-12 no-padding" src="./assets/images/products/ipadimage02.jpg" alt="ริบบิ้นผ้าดิบ">
					</div>
				</div>

				<div class="row row-image no-padding">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/71299781_2390255551223303_564721395502678016_o.jpg" alt="ริบบิ้นผ้าดิบ">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/67176710_2342096122705913_6144187172057513984_o.jpg" alt="ริบบิ้นผ้าดิบ">
				</div>

				<div class="row row-image no-padding">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/67418617_2342096046039254_8785662341942345728_n.jpg" alt="ริบบิ้นผ้าดิบ">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/69320537_2361156224133236_497430205845995520_o.jpg" alt="ริบบิ้นผ้าดิบ">
				</div>

				<div class="row row-image">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/52126753_2240573729524820_1375591885044711424_n.jpg" alt="ริบบิ้นผ้าดิบ">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/51882877_2240573712858155_4477419439469363200_n.jpg" alt="ริบบิ้นผ้าดิบ">
				</div>

				<div class="row row-image">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/60484605_2292422114339981_8870341732466688000_n.jpg" alt="ริบบิ้นผ้าดิบ">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/60221621_2292422057673320_2001305531297300480_n.jpg" alt="ริบบิ้นผ้าดิบ">
				</div>

				<div class="row row-image">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/52599378_2240573646191495_668358930585354240_n.jpg" alt="ริบบิ้นผ้าดิบ">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/hiplant.jpg" alt="ริบบิ้นผ้าดิบ">
				</div>

				<div class="row row-image">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/354235337_744235627541870_3171467330761320996_n.jpeg" alt="ริบบิ้นผ้าดิบ">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/354435423_744235630875203_948116084219107893_n.jpeg" alt="ริบบิ้นผ้าดิบ">
				</div>


				<div class="row row-image">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/354430854_744235587541874_5266100599997654099_n.jpeg" alt="ริบบิ้นผ้าดิบ">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/354071555_744235527541880_2988225556306718193_n.jpeg" alt="ริบบิ้นผ้าดิบ">
				</div>


				<div class="row row-image">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/354186111_744235484208551_8391401654754099366_n.jpeg" alt="ริบบิ้นผ้าดิบ">
					<img class="col-xs-12 col-sm-6 no-padding" src="./assets/images/products/354182344_744235457541887_1780687022266311303_n.jpeg" alt="ริบบิ้นผ้าดิบ">
				</div>
			</span>
		</div>
	</section>
	
	<?php include('footer.php')?>

	<script src="assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.bxslider.min.js"></script>

	<link rel="stylesheet" href="assets/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/jquery.fancybox.pack.js?v=2.1.5"></script>

	<!-- Optionally add helpers - button, thumbnail and/or media -->
	<link rel="stylesheet" href="assets/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<link rel="stylesheet" href="assets/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

	<script type="text/javascript">
	

	$(document).ready(function() {
		$('.bxslider').bxSlider({
			mode: 'fade'
		});
		$('body').scrollspy({ target: '.navbar-collapse' })
		$('.fancybox-buttons').fancybox({
			openEffect  : 'elastic',
			closeEffect : 'elastic',

			prevEffect : 'none',
			nextEffect : 'none',

			closeBtn  : false,

			helpers : {
				title : {
					type : 'inside'
				},
				buttons	: {}
			},

			afterLoad : function() {
				this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
			}
		});
	});
	</script>

	<script type='text/javascript'>
	window.__lo_site_id = 67913;
	(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
	})();
	</script>
	<script>
		(function(h,e,a,t,m,p) {
		m=e.createElement(a);m.async=!0;m.src=t;
		p=e.getElementsByTagName(a)[0];p.parentNode.insertBefore(m,p);
		})(window,document,'script','https://u.heatmap.it/log.js');
	</script>
	</body>
	</html>