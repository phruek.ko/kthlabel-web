<?php require_once 'preload.php'?>
<?php $category = htmlspecialchars($_GET['category'])?>
<!DOCTYPE html>
<html>
<head>
	<title>KTHLabel รับพิมพ์ริบบิ้น ป้ายตราเสื้อ ริบบิ้นพิมพ์ ริบบิ้นผูกของขวัญ </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="Keywords" content="รับพิมพ์ริบบิ้น,ป้ายตราเสื้อ,ริบบิ้นพิมพ์	,พิมพ์ริบบิ้น,พิมพ์ตราเสื้อ,ริบบิ้นผูกของขวัญ,พิมพ์โลโก้">
	<meta name="Description" content="รับพิมพ์ริบบิ้น,ริบบิ้นพิมพ์,ริบบิ้นผ้า,โบว์พิมพ์ริบบิ้น,ป้ายทอตราเสื้อ โบว์ผูกของขวัญ">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="stats-in-th" content="f55e">
	<meta name="languege" content="Thai">
	<meta name="distribution" content="Global">
	<meta name="rating" content="General">
	<meta name="area" content="Creating">
	<meta name="resource-type" content="Document">
	<meta name="revisit-after" content="1 Days">
	<meta name="placename" content="Thailand">
	<meta name="expires" content="none">
	<meta http-equiv="cache-control" content="max-age=31557600" />
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/jquery.bxslider.css">
	<link rel="stylesheet" href="assets/css/main.css">
	<link rel="stylesheet" href="assets/css/filter.css">
	<?php 
		if ( $detect->isMobile() ) {
			echo '<link rel="stylesheet" href="assets/css/mobile.css">';
		}
	?>
	<link rel="stylesheet" href="assets/css/product.css">
	<link rel="shortcut icon" href="assets/images/logo-black.ico">
	
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-TLBT7EYEND"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-TLBT7EYEND');
	</script>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '412707632792439');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=412707632792439&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WSDW9BH');</script>
	<!-- End Google Tag Manager -->
</head>

<body data-spy="scroll" data-target=".nav">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WSDW9BH"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div class="fb-customerchat" page_id="1397690197146515" minimized="true">
	</div>
	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function() {
			FB.init({
			appId            : '985230481604246',
			autoLogAppEvents : true,
			xfbml            : true,
			version          : 'v3.3'
			});
		};

		(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v3.3&autoLogAppEvents=1";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>
	
	<!-- Top Bar -->
	<?php require('topbar.php')?>
	<!-- End of Top Bar -->
	<!-- What is kthlabel -->
	
	<section id="product-list">
		<div class="container filter">
			<div class="row">
				<div id="myBtnContainer" class="col-xs-12">
					<button class="btn <?php echo $category == null ? 'action' : ''?>" onclick="filterSelection('all')">Show all</button>
					<button class="btn <?php echo $category == 'Border_satin' ? 'action' : ''?>" onclick="filterSelection('Border_satin')">Border satin</button>
					<button class="btn <?php echo $category == 'Satin' ? 'action' : ''?>" onclick="filterSelection('Satin')">Satin</button>
					<button class="btn <?php echo $category == 'Cream_cotton' ? 'action' : ''?>" onclick="filterSelection('Cream_cotton')">Cream cotton</button>
					<button class="btn <?php echo $category == 'White_cotton' ? 'action' : ''?>" onclick="filterSelection('White_cotton')">White cotton</button>
					<button class="btn <?php echo $category == 'Cotton' ? 'action' : ''?>" onclick="filterSelection('Cotton')">Cotton</button>
					<button class="btn <?php echo $category == 'Organdy_ribbon' ? 'action' : ''?>" onclick="filterSelection('Organdy_ribbon')">Organdy ribbon</button>
					<button class="btn <?php echo $category == 'Grosgrain' ? 'action' : ''?>" onclick="filterSelection('Grosgrain')">Grosgrain</button>
				</div>
			</div>
		</div>
		<div class="container">
			<?php 
				$strJsonFileContents = file_get_contents("products.json");
				$array = json_decode($strJsonFileContents, true);
			?>
			<?php foreach($array as $key => $value):?>
				<div class="element col-sm-4 col-md-4 col-xs-12 filterDiv <?php echo $value['ribbonType']?>">
					<div class="box brd_rad">
						<div class="img">
							<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo $value["ImageUrl"]?>" alt="<?php echo $value['ProductNameTH']?>">
								<img class="lazy brd_rad_top" data-src="<?php echo $value['ImageUrl']?>" class="" alt="<?php echo $value['ProductNameTH']?>">
							</a>
						</div>
						<div class="topic"><?php echo $value['ProductNameTH']?></div>
						<div class="detail hide"></div>
					</div>
				</div>
			<?php endforeach;?>
		</div>
	</section>
	
	<?php include('footer.php')?>

	<script src="assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.bxslider.min.js"></script>

	<link rel="stylesheet" href="assets/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/jquery.fancybox.pack.js?v=2.1.5"></script>

	<!-- Optionally add helpers - button, thumbnail and/or media -->
	<link rel="stylesheet" href="assets/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<link rel="stylesheet" href="assets/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript" src="./assets/source/jquery.lazy-master/jquery.lazy.min.js"></script>
	<script type="text/javascript">
	
	
	$(document).ready(function() {
		$('.bxslider').bxSlider({
			mode: 'fade'
		});
		$('body').scrollspy({ target: '.navbar-collapse' })
		$('.fancybox-buttons').fancybox({
			openEffect  : 'elastic',
			closeEffect : 'elastic',

			prevEffect : 'none',
			nextEffect : 'none',

			closeBtn  : false,

			helpers : {
				title : {
					type : 'inside'
				},
				buttons	: {}
			},

			afterLoad : function() {
				this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
			}
		});
		filterSelection("<?php echo $category?>")
	});
	$('.lazy').Lazy();
	
	</script>

	<script type='text/javascript'>
	window.__lo_site_id = 67913;
	(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
	})();
	</script>
	<script>
		(function(h,e,a,t,m,p) {
		m=e.createElement(a);m.async=!0;m.src=t;
		p=e.getElementsByTagName(a)[0];p.parentNode.insertBefore(m,p);
		})(window,document,'script','https://u.heatmap.it/log.js');

               
	</script>

	<script>
		
		function filterSelection(c) {
		  var x, i;
		  x = document.getElementsByClassName("filterDiv");
		  if (c == "all") c = "";
		  // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
		  for (i = 0; i < x.length; i++) {
			w3RemoveClass(x[i], "show");
			if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
		  }
		}
		
		// Show filtered elements
		function w3AddClass(element, name) {
		  var i, arr1, arr2;
		  arr1 = element.className.split(" ");
		  arr2 = name.split(" ");
		  for (i = 0; i < arr2.length; i++) {
			if (arr1.indexOf(arr2[i]) == -1) {
			  element.className += " " + arr2[i];
			}
		  }
		}
		
		// Hide elements that are not selected
		function w3RemoveClass(element, name) {
		  var i, arr1, arr2;
		  arr1 = element.className.split(" ");
		  arr2 = name.split(" ");
		  for (i = 0; i < arr2.length; i++) {
			while (arr1.indexOf(arr2[i]) > -1) {
			  arr1.splice(arr1.indexOf(arr2[i]), 1);
			}
		  }
		  element.className = arr1.join(" ");
		}
		
		// Add active class to the current control button (highlight it)
		var btnContainer = document.getElementById("myBtnContainer");
		var btns = btnContainer.getElementsByClassName("btn");
		for (var i = 0; i < btns.length; i++) {
		  btns[i].addEventListener("click", function() {
			var current = document.getElementsByClassName("action");
			current[0].className = current[0].className.replace(" action", "");
			this.className += " action";
		  });
		}
		
	</script>

	</body>
</html>